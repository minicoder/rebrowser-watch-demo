/**
 * OverView: 程序主入口
 * Author: Zhaomi
 * Date: 13-4-17 下午8:36
 * Email: 852632338@qq.com
 * Create By JetBrains WebStorm.
 */
document.write("entry run now<br/>");

var System = require("./libs/System.js");
var Imports = require("./libs/Imports.js");

if(window.location.href.indexOf("mine") > 0){
    var TestApp = Imports("./hello.js");

    TestApp.run();
}

if(window.location.href.indexOf("yun") > 0){
    var TestApp = Imports("./world.js");

    TestApp.run();
}