/**
 * OverView: 程序主入口
 * Author: Zhaomi
 * Date: 13-4-17 下午8:36
 * Email: 852632338@qq.com
 * Create By JetBrains WebStorm.
 */
var System = require("./libs/System.js");
var Math = require("./libs/Math.js");
var Imports = require("./libs/Imports.js");
System.print("read html: " + Imports("text!main.html"));
System.print("add result " + Math.add(8, 6));