var Rebrowser = require('rebrowser-watch');

/**
 * Rebrowser Source Code --- Compress Switch
 * */
Rebrowser.setCompressState(1);
/**
* define app entrance and various page apps that have been writed by the nodejs style, package them to browserify files
* @param {jsfiles} define file path that should been packaged
* @param {cb} define the callback that executed when files are packaged successfully
* @param {excludeMap} define specific files that should be skiped to scanning and adding to output when packaged file generating
* ...
*/
Rebrowser.combineWatcher(
   "./app.js", // define the path of app entrance
    /**
    * define the callback that executed when app entrance file packages successfully
    * @param fmap , the map that contains filename and filecode
    * @example {"F:\\MyWork\\Web-Front\\browserify-test\\src\\libs\\System.js": 695}
    * ...
    */
   function(fmap){
        Rebrowser.combineWatcher(
           // define paths of various page apps that would be called by app entrance dynamically
           ["./hello.js","./world.js"],
           null,
           fmap
       );
   }
);