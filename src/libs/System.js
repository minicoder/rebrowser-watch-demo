/**
 * OverView: 运行环境输入输出函数库
 * Author: Zhaomi
 * Date: 13-4-17 下午8:32
 * Email: 852632338@qq.com
 * Create By JetBrains WebStorm.
 */
var plain = document.getElementsByTagName('body')[0];
exports.print =function(msg){
    console.log(">>> Eventw <<< " +msg);
    var spanNode = document.createElement('span');
    spanNode.style.display = "block";
    spanNode.innerText = ">>> Event <<< " + msg;
    plain.appendChild(spanNode);
};