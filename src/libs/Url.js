var _queryMap;

function _buildMap(){
    _queryMap = {};

    var pos = -1;
    var currPageUrl = window.location.href;

    if((pos = currPageUrl.indexOf("?")) < 0)
        return;

    var queryString = decodeURIComponent(currPageUrl.substr(pos+1));
    queryString = queryString.replace(/#+$/,'');

    var hary = queryString.split(/=|&/);

    for(var k = 0;hary.length - k > 1;k += 2){
        var val = hary[k+1];
        if(val != undefined){
            if(/^[\d\.]+$/.test(val))
                if(val.indexOf(".")<0)
                    val = parseInt(val);
                else
                    val = parseFloat(val);
            if("true" == val || "false" == val)
                val= (val == "true");
        }
        _queryMap[hary[k]] = val;
    }
}

exports.getUrlVal = function(key){
    if('undefined' == typeof(_queryMap)){
        _buildMap();
    }
    return _queryMap[key];
}

exports.removeKeyFromUrl = function(url,key){
    if(!url || !key)
        return url;
    var regx = eval("/(\\?|&)" + key + "=[^&]+(&)?/ig");
    return url.replace(regx,function(match,c1,c2){
        if(c2 && "?" == c1)
            return "?";
        return "&";
    });
}





