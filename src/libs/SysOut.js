function _log(type, msg){
    var output = '>>> ' + type + ' <<< <font size="2">' + msg + '</font>';
    var spanColor = '#333333';

    if('Error' ==  type){
        console.error(output);
        spanColor = '#ff0000';
    }else if('Warn' == type){
        console.warn(output);
        spanColor = '#c3c3c3';
    }else{
        console.log(output);
    }

    document.write('<span style="color:' + spanColor + ';font-size:14px;">' + output + '</span></br>')
}

exports.event = function(msg){
    _log("Event", msg);
};

exports.warn = function(msg){
    _log("Warn", msg);
};

exports.error = function(msg){
    _log("Error", msg);
};
