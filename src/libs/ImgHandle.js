// 将图片链接转成高清链接格式
exports.img2HD = function(url) {
    var pos = url.lastIndexOf("_.");
    if (pos < 0) {
        return url;
    }
    return url.substring(0, pos) + url.substring(pos + 1);
};

// 将图片链接转成缩略图格式
exports.img2Thum = function(url) {
    var pos = url.lastIndexOf(".");
    if (url.substr(pos - 1, 1) == "_") {
        return url;
    }

    return url.substring(0, pos) + "_" + url.substring(pos);
};

// 截取字符串中指定前后标志的字符
exports.cutString = function(str, startFlag, endFlag) {
    var startpos = 0;
    if (null != startFlag || startFlag.length != 0) {
        startpos = str.lastIndexOf(startFlag);
        if (startpos == -1) {
            startpos = 0;
        } else {
            startpos += startFlag.length;
        }
    }
    var endpos = str.length;
    if (null != endFlag || endFlag.length != 0) {
        endpos = str.lastIndexOf(endFlag);
        if (endpos == -1) {
            endpos = str.length;
        }
    }
    return str.substring(startpos, endpos);
};

/*根据图片URL生成图片缓存地址，需要截取出图片ID
 param logourl 图片网络地址
 param dimformat 忽略网络环境强制使用压缩图
 如果在无图模式下 return 空字符串
 如果在有图且WiFi环境下 return 高清图
 如果在有图且非WiFi环境下 return 压缩图
 */
function imgCacheGent(logourl , dimformat){
    if(NoImgsState)
        return "";

    if(!dimformat && WiFiNetWorkState){
        logourl = exports.img2HD(logourl);
    }else{
        logourl = exports.img2Thum(logourl);
    }

    if(DebugInPCBrowser)
       return logourl;

    var tpos=logourl.lastIndexOf("/");
    if(tpos < 0 || tpos+1 == logourl.length)
        return logourl;

    var sufix=logourl.substr(tpos+1);
    return ImgCacheUrl.format(sufix,logourl);
}

