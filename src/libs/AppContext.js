


//获取屏幕真实宽高，通常状况下用于做自适应
var _realScreenSizeObj = (function(){
    var itemkey = "ni_screen_real_size";
    var sizeStr;

    /*首先从LocalStorage获取*/
    if(window.localStorage != null && (sizeStr = localStorage.getItem(itemkey)))
        return JSON.parse(sizeStr);

    /*再从phoneGap中获取*/
    sizeStr = NineGameClient.getScreenDimension();

    /*还未获取成功则,用原生脚本获取宽高,失败则用默认值*/
    if(!sizeStr)
        sizeStr = '{"width":'+ (doc.documentElement.clientWidth || 320) +',"height":'+ (doc.documentElement.clientHeight || 480) +'}';
    if(exports.IS_LOCALSTORAGE_ENABLED)
        localStorage.setItem(itemkey,sizeStr);
    return JSON.parse(sizeStr);
})();

exports.IS_LOCALSTORAGE_ENABLED = window.localStorage != null;
exports.DEFAULT_SHOTIMG_URL = 'css/images/defaultshot.png';
exports.REAL_SCREEN_WIDTH = _realScreenSizeObj.width;
exports.REAL_SCREEN_HEIGHT = _realScreenSizeObj.height;
exports.IMAGE_CACHE_PATH = (nativeApp.ImgCacheDir || 'file:///sdcard/Android/data/cn.ninegame.gamemanager/cache/images') + '/{0}?url={1}';
exports.NO_IMAGES_STATE = false;
exports.NETWORK_TYPE = '3g';
exports.IS_WIFI_NETWORK_STATE = false;


_configureReset(_getConfigures()); /*初始设值*/

function _getConfigures(){    //获取用户配置选项
    var configureStr;

    /*从phoneGap中获取*/
    configureStr = nativeApp.getSettingProperties();

    /*还未获取成功则,用原生脚本获取配置,失败则用默认值*/
    if(!configureStr)
        configureStr = "{}";

    return JSON.parse(configureStr);
}

nativeApp.registerSettingChangeListener(function(confObj){
    _configureReset(confObj);
});

/*配置选项的二次设值*/
function _configureReset(confObj){

    //全局控制是否开启省流量无图模式,1表示打开,默认0表示关闭
    if(confObj["no_images"]){
        exports.NO_IMAGES_STATE = confObj['no_images'];
    }

    //用户当前网络环境发生变化
    if(confObj["network_type"]){
        exports.NETWORK_TYPE = confObj['network_type'];
        exports.IS_WIFI_NETWORK_STATE = "wifi" == exports.NETWORK_TYPE;
    }

}

exports.getAppUuid = function(){
    return nativeApp.getEnvAttr("uuid");
}

exports.getAppImei = function(){
    return nativeApp.getEnvAttr("imei");
}

exports.getAppModel = function(){
    return nativeApp.getEnvAttr("model");
}

exports.getAppMac = function(){
    return nativeApp.getEnvAttr("mac");
}

exports.getAppVersion = function(){
    return nativeApp.getEnvAttr("apkversion");
}

exports.getAppVersionCode = function(){
    return nativeApp.getEnvAttr("versioncode");
}

exports.getAppTplVersion = function(){
    return nativeApp.getEnvAttr("templateversion");
}

exports.getAppSysVersion = function(){
    return nativeApp.getEnvAttr("systemversion");
}
