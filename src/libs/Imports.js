var NativeApp = require("../phonegap/native-bridge.js");

var global = window;
var _head = document.getElementsByTagName('head')[0];
var _pathSet = [];
var _classSet = [];

/**依赖于9game目录名，寻找程序运行的绝对路径的前缀
 * 如后期9game目录名发生改变，此处的字符也做相应改变
 * 注：PC浏览器调试时文件定位使用
 * */
var _appRunDirectory = '/src/';
var _mainPagePath = global.location.href.substring(0, global.location.href.lastIndexOf("/"));

var _config = {

    /*包引用时使用的别名、短名定义*/
    'shortAlias': {},

    /*文件请求时携带的URL参数后缀*/
    'pathArgs': null

};

function _extend(srcObj,extObj){
    for(var key in srcObj){
        if(!extObj[key])    //目标中不存在则将其补充在目标中
            extObj[key] = srcObj[key];
    }
    return extObj;
}
/**
 * 相对路径 解释成 绝对路径
 * */
function _getFileFullPath(path){
    var directoty = _mainPagePath;
    var pIdx;
    while((pIdx = path.indexOf("../")) >= 0){
        path = path.substr(pIdx + 2);
        directoty = directoty.substring(0, directoty.lastIndexOf("/"));
    }
    if(path.length > 2 && path.substr(0,2) == "./"){
        path = path.substr(1);
    }else if(path.indexOf("/") < 0){
        path = "/" +path;
    }
    path = directoty + path;

    /* *
     * 手机调试模式下，使用文件相对路径，相对于html解压目录，此目录下一层则是 应该是根目录应该是 /9game/
     * */
    if(global.IS_MOBILE_MODE){
        return path.substr(path.indexOf(_appRunDirectory));
    }
    return _config.pathArgs ? path + "?" + _config.pathArgs : path;
}

function _getFileContent(path){
    path = _getFileFullPath(path);

    /* *
     *手机上使用壳接口 readFileSync 同步读取文件内容，PC上使用Xhr跨域实现同步获取
     * */
    if(global.IS_MOBILE_MODE){
        return  NativeApp.readFileSync(path, 'utf-8')
    }

    var xhrObj = new XMLHttpRequest();
    xhrObj.open('GET', path, false);
    xhrObj.send('');
    return xhrObj.responseText;
}

function _getClassFromCache(pathKey){
    var pathSetIdx = _pathSet.indexOf(pathKey);

    if(pathSetIdx < 0 || pathSetIdx >= _classSet.length)
        return null;

    return _classSet[pathSetIdx];
}

function _getPackedTextInPath(path){

    var source = _getFileContent(path);

    return 'delimit(function(){\nvar exports = {};\n' +
        'exports.$pathKeyN309 = \'' + path + '\';\n' +
        source +
        '\nreturn exports;\n});';
}

function _convertPath(path){
    var lastIdx = path.lastIndexOf("/");
    var pointIdx = path.lastIndexOf(".");

    return path.substring(0,lastIdx) + "/run/" + path.substring(lastIdx + 1, pointIdx) + ".run" +path.substr(pointIdx);
}

/**模块引入函数定义
 * @param path 相对于_absolutePath 值的 相对路径 如此处：'file:///E:/projects/Web-Front/android-webapp/src/'， 可以使用别名
 * 暂时 path以文件当前所在路径为查找依据
 */
module.exports = function (path) {
    "use strict"; //进入严格模式的标志 http://www.ruanyifeng.com/blog/2013/01/javascript_strict_mode.html

    /*查找映射表*/
    if(_config.shortAlias[path]){
        path = _config.shortAlias[path];
    }

    /*纯文本内容状态*/
    if(path.length > 5 && 'text!' == path.substr(0, 5)){
        return _getFileContent(path.substr(5));
    }
    /**
     * 为了符合Rebrowser体系，将文件引用路径做一次转换
     * 即多一层文件夹 run 目录，名称后缀改为 .run.js
     * */
    path = _convertPath(path);

    var rtnClass = _getClassFromCache(path);

    if(null != rtnClass)
        return rtnClass;

    /*create script node element*/
    var scriptNode = document.createElement('script');
    scriptNode.type =  'text/javascript';
    scriptNode.charset = 'utf-8';

    scriptNode.text = _getPackedTextInPath(path);
    _head.appendChild(scriptNode);

    return _getClassFromCache(path);
};
/**
 module.imports.config = function(conf){
                _config = _extend(_config, conf);
            };
 */
global.delimit = function (callback) {
    var classObj = callback.apply();
    var pathKey = classObj["$pathKeyN309"];

    if("undefined" != typeof(pathKey)){
        _pathSet.push(pathKey);
        _classSet.push(classObj);
    }
};